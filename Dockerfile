FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY . /code/
RUN apk add --update --no-cache g++ gcc libxslt-dev
RUN pip install -r requirements.txt
RUN [ "python", "-c", "import nltk; nltk.download('stopwords')" ]
