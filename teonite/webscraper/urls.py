from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('authors', views.AuthorView)

urlpatterns = [
    path('', views.index, name='index'),
    path('api/', include(router.urls)),
    path('stats/', views.content_stats, name='stats'),
    path('stats/<str:author_name>', views.authors_stats, name='author_stats')

]
