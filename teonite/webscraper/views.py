from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import HttpResponse
from django.http import JsonResponse
from .models import Author, Content
from bs4 import BeautifulSoup
from rest_framework import viewsets
from .serializers import AuthorSerializer
from collections import Counter
from nltk.corpus import stopwords
import requests
import urllib3.request

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def index(request):
    session = requests.Session()
    session.headers = {"User-Agent: Mozilla/5.0"}
    url = 'https://teonite.com/blog/'
    content = session.get(url, verify=False).content
    soup = BeautifulSoup(content, "lxml")
    posts = soup.find_all('div', {'class': 'post-container'})  # returns a list
    for i in posts:
        link = i.find_all('a', {'class': 'blog-button post-read-button post-button-animation'})[0]
        url = link.get('href')  # getting the url of each post
        fixed_url = 'https://teonite.com/' + url
        content = session.get(fixed_url, verify=False).content
        soup = BeautifulSoup(content, "lxml")
        author = soup.find_all('span', {'class': 'author-name'})[0].text  # getting the author name
        description = soup.find_all('div', {'class': 'post-content'})[0].text  # getting the content of post

        # Creating a Author and Content model from soup data
        try:
            a = Author.objects.get(name=author)
            Content.objects.get_or_create(description=description, author=a)
        except ObjectDoesNotExist:
            author_name = author
            author = Author.objects.create(name=author_name)
            author.save()
            Content.objects.get_or_create(description=description, author=author)

    return HttpResponse('<a href="/api/authors/">Authors</a><br>'
                        '<a href="/stats">Stats</a><br>')


def content_stats(request):
    counters = []
    for content in Content.objects.all():
        stop_words = set(stopwords.words('english'))
        counters.append(Counter(word for word in content.description.split() if word.lower() not in stop_words))
        total_counter = sum(counters, Counter())
        most = dict(total_counter.most_common(10))
    return JsonResponse(most)
    # Returns most used words from Content Model


def authors_stats(request, author_name):
    author_name = Author.objects.get(name=author_name)
    author_content = Content.objects.filter(author=author_name)
    counters = []
    for content in author_content:
        stop_words = set(stopwords.words('english'))
        counters.append(Counter(word for word in content.description.split() if word.lower() not in stop_words))
        total_counter = sum(counters, Counter())
        most = dict(total_counter.most_common(10))
    return JsonResponse(most)
    # Returns most used word from Content Model by Author instance


class AuthorView(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
